import config from 'config'
import rp from 'request-promise'

export class AlertService {
  private static readonly isProduction = process.env.NODE_ENV === 'production'

  public static async sendMessage(message: any) {
    if (!this.isProduction) return true
    const url = `${config.get<string>('telegram.serviceUrl')}/api/telegram/send`
    return rp(url, {
      method: 'POST',
      body: { text: message },
      json: true,
      headers: {
        'x-access-token': 'jdsg536!fdsjGDS64?@uij&jgklnG214-1214hjjkLL!',
      },
    })
  }
}
