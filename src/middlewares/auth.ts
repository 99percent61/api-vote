import { sendError } from '../lib/utils'
import { decodeToken } from '../lib/auth'
import DB from '../lib/db'

export default async function (req, res, next) {
  const LOGGER_PREFIX = 'Authentication: '
  const db = new DB()
  const token = req.headers['x-access-token'] || req.headers.authorization
  if (!token) {
    return sendError({
      res,
      message: 'Access denied.',
      code: 401,
      LOGGER_PREFIX,
    })
  }

  try {
    const decoded = decodeToken(token)
    const user = await db.getUserById(decoded.id)
    if (!user) {
      return sendError({
        res, code: 400, message: 'User does not found', LOGGER_PREFIX,
      })
    }
    if (user.is_blocked) {
      return sendError({
        res, code: 403, message: 'User is blocked', LOGGER_PREFIX,
      })
    }
    res.locals.user = user
    return next()
  } catch (error) {
    return sendError({
      res, code: 400, message: 'Invalid token', LOGGER_PREFIX,
    })
  }
}
