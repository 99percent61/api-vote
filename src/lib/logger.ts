import bunyan from 'bunyan'
import bunyanLogstash from 'bunyan-logstash'
import config from 'config'

const logger = bunyan.createLogger({
  name: config.get('logger.name'),
  streams: [
    {
      stream: process.stdout,
    },
    {
      type: 'raw',
      stream: bunyanLogstash.createStream({
        server: config.get('logger.server'),
        host: config.get('logger.host'),
        port: config.get('logger.port'),
        appName: config.get('logger.appName'),
      }),
      reemitErrorEvents: true,
    },
  ],
})

logger.on('error', (err) => {
  console.error(err)
})

export default logger
