import * as securePin from 'secure-pin'

export default class Pin {
  public generate(number: number = 5): string {
    const pin = securePin.generatePinSync(number)
    return pin
  }
}
