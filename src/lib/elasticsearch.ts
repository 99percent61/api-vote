import { Client } from 'elasticsearch'
import config from 'config'
import logger from './logger'

export type Bucket = {
  key: number
  doc_count: number
}

export default class ES {
  private esClient: Client

  private LOGGER_PREFIX = 'ES client: '

  constructor() {
    this.esClient = new Client({
      host: config.get('elasticsearch.host'),
      requestTimeout: config.get('elasticsearch.timeout'),
      keepAlive: false,
      log: 'debug',
    })
  }

  public async search(query): Promise<any> {
    try {
      return this.esClient.search(query)
    } catch (error) {
      logger.error(this.LOGGER_PREFIX, error)
      throw new Error(error)
    }
  }

  public async update(query): Promise<any> {
    try {
      return this.esClient.update(query)
    } catch (error) {
      logger.error(this.LOGGER_PREFIX, error)
      throw new Error(error)
    }
  }

  public async createDocument(
    { document, indexName, entityType }: { document: any, indexName: string, entityType: string },
  ): Promise<any> {
    try {
      return this.esClient.create({
        id: document.id,
        index: indexName,
        type: entityType,
        body: document,
        refresh: true,
      })
    } catch (error) {
      logger.error(this.LOGGER_PREFIX, error)
      throw new Error(error)
    }
  }

  public async delete(query: any): Promise<any> {
    try {
      return this.esClient.delete(query)
    } catch (error) {
      logger.error(this.LOGGER_PREFIX, error)
      throw new Error(error)
    }
  }

  public async stats(): Promise<any> {
    try {
      return this.esClient.indices.stats({
        index: '_all',
      })
    } catch (error) {
      logger.error(this.LOGGER_PREFIX, error)
      throw new Error(error)
    }
  }

  public async existIndex(name: string): Promise<boolean> {
    try {
      return this.esClient.indices.exists({
        index: name,
      })
    } catch (error) {
      logger.error(this.LOGGER_PREFIX, error)
      throw new Error(error)
    }
  }

  public async createIndex(name: string): Promise<any> {
    try {
      return this.esClient.indices.create({
        index: name,
      })
    } catch (error) {
      logger.error(this.LOGGER_PREFIX, error)
      throw new Error(error)
    }
  }

  public async deleteIndex(name: string): Promise<any> {
    try {
      return this.esClient.indices.delete({
        index: name,
      })
    } catch (error) {
      logger.error(this.LOGGER_PREFIX, error)
      throw new Error(error)
    }
  }

  public async putIndexAlias(
    { alias, indexName }: { alias: string, indexName: string },
  ): Promise<any> {
    try {
      return this.esClient.indices.putAlias({
        index: indexName,
        name: alias,
      })
    } catch (error) {
      logger.error(this.LOGGER_PREFIX, error)
      throw new Error(error)
    }
  }

  public async putMapping(
    { name, entityType, mapping }
    : { name: string, entityType: string, mapping: Record<string, any> },
  ): Promise<any> {
    try {
      return this.esClient.indices.putMapping({
        index: name,
        type: entityType,
        body: mapping,
        includeTypeName: true,
      })
    } catch (error) {
      logger.error(this.LOGGER_PREFIX, error)
      throw new Error(error)
    }
  }

  public close(): void {
    this.esClient.close()
  }

  public extractSourceFromHits(hits: any[]): any[] {
    return hits.map((hit) => hit._source)
  }
}
