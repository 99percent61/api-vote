import jwt from 'jsonwebtoken'
import config from 'config'
import CryptoJS from 'crypto-js'

function generateToken(userId: number): string {
  const token = jwt.sign({ id: userId }, config.get('user.auth.secretKey'))
  return token
}

function decodeToken(token: string): { id: number } {
  const decoded = jwt.verify(token, config.get('user.auth.secretKey'))
  return decoded
}

function encryptPassword(password: string): string {
  const encriptedPassword = CryptoJS.AES.encrypt(password, config.get('db.secretKey')).toString()
  return encriptedPassword
}

function decryptPassword(password: string): string {
  const decryptedPassword = CryptoJS.AES.decrypt(password, config.get('db.secretKey')).toString(CryptoJS.enc.Utf8)
  return decryptedPassword
}

function isEqualPasswords(
  { clientPassword, dbPassword }: { clientPassword: string, dbPassword: string },
): boolean {
  return clientPassword === dbPassword
}

export {
  generateToken,
  decodeToken,
  encryptPassword,
  decryptPassword,
  isEqualPasswords,
}
