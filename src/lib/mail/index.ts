import nodemailer, { TestAccount } from 'nodemailer'
import Mail from 'nodemailer/lib/mailer'
import EmailTemplates from 'email-templates'
import config from 'config'
import logger from '../logger'

export default class Mailer {
  private transporter: Mail

  private testAccount: TestAccount

  private info: Promise<any>

  private LOGGER_PREFIX = 'MAILER: '

  private email: EmailTemplates

  constructor() {
    this.email = new EmailTemplates({
      message: {},
    })
  }

  public async createTestAccount(): Promise<void> {
    this.testAccount = await nodemailer.createTestAccount()
  }

  public createTransport({ user, password }: { user?: string, password?: string } = {}): void {
    const localUser = user || config.get('mailer.user.email')
    const localPassword = password || config.get('mailer.user.password')

    this.transporter = nodemailer.createTransport({
      host: config.get('mailer.host'),
      port: config.get('mailer.port'),
      secure: true,
      auth: {
        user: localUser,
        pass: localPassword,
      },
    })
  }

  public async send({
    to, subject, html,
  }: { to: string, subject: string, html: string }): Promise<void> {
    if (!to || !subject || !html) {
      throw new Error('Missing required field')
    }

    try {
      this.info = await this.transporter.sendMail({
        from: config.get('mailer.user.email'),
        to,
        subject,
        html,
      })
    } catch (error) {
      logger.error(this.LOGGER_PREFIX, error)
    }
  }

  public getInfo(): any {
    return this.info
  }

  public async renderTemplate(template: string, locals: Record<string, any>): Promise<string> {
    let html = ''
    try {
      html = await this.email.render(`templates/${template}`, locals)
    } catch (error) {
      logger.error(this.LOGGER_PREFIX, error)
    }
    return html
  }
}
