import { Joi } from 'express-validation'

const login = Joi.string()
  .min(3)
  .max(50)
  .required()

const password = Joi.string()
  .min(8)
  .max(50)
  .required()

const email = Joi.string()
  .email()
  .required()

const name = Joi.string()
  .min(2)
  .max(50)
  .required()

const sex = Joi.string()
  .pattern(/^male$|^female$/)
  .required()

const age = Joi.number()
  .integer()
  .min(5)
  .max(130)
  .required()

const pinCode = Joi.string()
  .min(5)
  .max(5)
  .required()

const answer = Joi.object({
  id: Joi.number().integer().required(),
  title: Joi.string().required(),
  votes: Joi.number().integer(),
})

const answers = Joi.array()
  .items(answer)

const image = Joi.alternatives()
  .try(
    Joi.string(),
    Joi.array().items(Joi.string()),
  )

const interger = Joi.number()
  .integer()

const requiredInteger = Joi.number()
  .integer()
  .required()

const string = Joi.string()

const requiredString = Joi.string()
  .required()

const page = Joi.number()
  .integer()
  .min(1)
  .required()

const size = Joi.number()
  .integer()
  .max(20)
  .required()

export {
  login,
  password,
  email,
  name,
  sex,
  age,
  pinCode,
  interger,
  requiredInteger,
  answer,
  answers,
  image,
  string,
  size,
  page,
  requiredString,
}
