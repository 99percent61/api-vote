import mysql from 'mysql'
import config from 'config'
import logger from './logger'
import { User } from '../routes/user/types'
import { Poll } from '../routes/poll/types'
import { Complaint } from '../routes/complaint/types'

export default class db {
  private host: string = config.get('db.host')

  private user: string = config.get('db.user')

  private password: string = config.get('db.password')

  private database: string = config.get('db.database')

  private connection: mysql.Connection

  private readonly LOGGER_PREFIX = 'MySQL connection: '

  constructor() {
    this.connection = mysql.createConnection({
      host: this.host,
      user: this.user,
      password: this.password,
      database: this.database,
    })

    this.connect()
  }

  private connect() {
    this.connection.connect((err) => {
      if (err) {
        logger.error(this.LOGGER_PREFIX, err)
        this.connect()
        return
      }
      logger.info(this.LOGGER_PREFIX, `connected as id ${this.connection.threadId}`)
    })
  }

  public query(query: string, data: any = null): Promise<any> {
    return new Promise((resolve, reject) => {
      this.connection.query(query, data, (error, result) => {
        if (error) {
          return reject(error)
        }
        return resolve(result)
      })
    })
  }

  public async update(
    { table, updateFields, key, values }
    : { table: string, updateFields: string[], key: string, values: any[] },
  ): Promise<void> {
    const buildedUpdateFields = updateFields.map((field) => `${field} = ?`).join(',')
    const query = `UPDATE ${table} SET ${buildedUpdateFields} WHERE ${key} = ?`
    await this.query(query, values)
  }

  public async delete(
    { table, key, value }: { table: string, key: string, value: number | string },
  ): Promise<any> {
    const query = `DELETE FROM ${table} WHERE ${key} = ?`
    return this.query(query, value)
  }

  public prepareRecord(record: Record<string, any>): Record<string, any> {
    const tempRecord = { ...record }

    Object.keys(tempRecord).forEach((key) => {
      tempRecord[key] = tempRecord[key] !== undefined ? tempRecord[key] : null
    })
    return tempRecord
  }

  public async getUserById(id: number): Promise<User | null> {
    const fileds = ['id', 'login', 'email', 'name', 'password', 'avatar', 'black_list', 'settings', 'is_blocked', 'sex', 'age', 'subscribers', 'subscriptions', 'terms']
    const record = await this.query(`SELECT ${fileds.join(',')} FROM users WHERE id = ?`, id)
    if (!record.length) {
      return null
    }
    const user = this.parseJsonFields({ record: record[0], fields: ['settings', 'subscribers', 'subscriptions', 'black_list'] })
    return user
  }

  public async getUserByEmail(email: string): Promise<User | null> {
    const fileds = ['id']
    const record = await this.query(`SELECT ${fileds.join(',')} FROM users WHERE email = ?`, email)
    if (!record.length) {
      return null
    }
    return record[0]
  }

  public async getPollById(id: number | string): Promise<Poll | null> {
    const record = await this.query('SELECT * FROM polls WHERE id = ?', id)
    if (!record.length) {
      return null
    }
    const poll = this.parseJsonFields({ record: record[0], fields: ['answers', 'image', 'author'] })
    return poll
  }

  public async getVoteById(id: number | string): Promise<Poll | null> {
    const record = await this.query('SELECT * FROM votes WHERE id = ?', id)
    if (!record.length) {
      return null
    }
    return record[0]
  }

  public async getComplaintById(id: number | string): Promise<Complaint | null> {
    const record = await this.query('SELECT * FROM complaints WHERE id = ?', id)
    if (!record.length) {
      return null
    }
    return record[0]
  }

  private parseJsonFields(
    { record, fields }: { record: Record<string, any>, fields: string[] },
  ): any {
    try {
      const copy = { ...record }
      fields.forEach((field) => {
        copy[field] = copy[field] && typeof copy[field] === 'string' ? JSON.parse(copy[field]) : copy[field]
      })
      return copy
    } catch (error) {
      logger.error(this.LOGGER_PREFIX, error)
      return record
    }
  }

  public close() {
    this.connection.end()
  }
}
