import { Joi } from 'express-validation'
import { email, pinCode, requiredString, password } from '../../../lib/validation'

const forgotValidation = {
  body: Joi.object({
    email,
  }),
}

const confirmCodeValidation = {
  body: Joi.object({
    email,
    code: pinCode,
  }),
}

const createPassByTokenValidation = {
  body: Joi.object({
    email,
    token: requiredString,
    password,
    repeatedPassword: Joi.ref('password'),
  }).with('password', 'repeatedPassword'),
}

const createPassByPassValidation = {
  body: Joi.object({
    currentPassword: password,
    newPassword: password,
    repeatedPassword: Joi.ref('newPassword'),
  }).with('newPassword', 'repeatedPassword'),
}

export {
  forgotValidation,
  confirmCodeValidation,
  createPassByTokenValidation,
  createPassByPassValidation,
}
