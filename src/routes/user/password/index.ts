import { Router } from 'express'
import { IConfig } from 'config'
import { validate } from 'express-validation'
import Elasticsearch from '../../../lib/elasticsearch'
import DB from '../../../lib/db'
import Mailer from '../../../lib/mail'
import { sendError, sendResponse } from '../../../lib/utils'
import Pin from '../../../lib/pin'
import { generateToken, encryptPassword, decryptPassword } from '../../../lib/auth'
import { forgotValidation, confirmCodeValidation, createPassByTokenValidation, createPassByPassValidation } from './validation'
import { savePinToDb, isSentCode, isEqualValues, updateRecordByEmail } from './helpers'
import auth from '../../../middlewares/auth'
import { User } from '../types'

export default ({ config, db }: { config?: IConfig, esClient?: Elasticsearch, db?: DB }) => {
  const passwordApi = Router()
  const LOGGER_PREFIX = 'FORGOT PASSWORD: '
  const mailer = new Mailer()
  const pin = new Pin()
  const validationOptions = {
    context: true,
    keyByField: true,
  }

  passwordApi.post('/forgot', validate(forgotValidation, validationOptions, {}), async (req, res) => {
    const { email } = req.body
    const pinCode = pin.generate()

    try {
      const alreadySent = await isSentCode({ db, email })
      if (!alreadySent) {
        await savePinToDb({ db, email, code: pinCode })
      } else {
        await updateRecordByEmail({ db, email, key: 'code', value: pinCode, table: 'codes' })
        await updateRecordByEmail({ db, email, key: 'is_confirmed_code', value: false, table: 'codes' })
      }

      mailer.createTransport()
      const html = await mailer.renderTemplate('pinCode', { pinCode })
      if (!html) {
        sendError({ res, message: 'Error rendering the email template', LOGGER_PREFIX })
        return
      }
      await mailer.send({ to: email, subject: 'Vote! Confirm code.', html })
      sendResponse({ res, result: [null, true] })
    } catch (error) {
      sendError({ res, message: error, LOGGER_PREFIX })
    }
  })

  passwordApi.post('/confirm-code', validate(confirmCodeValidation, validationOptions, {}), async (req, res) => {
    const { email, code } = req.body

    try {
      const result = await db.query('SELECT code, is_confirmed_code FROM codes WHERE email = ?', email)
      if (!result.length) {
        return sendError({ res, message: 'Wrong email', LOGGER_PREFIX, code: 400 })
      }
      const record = result[0]
      const isConfimedCode = record.is_confirmed_code
      if (isConfimedCode) {
        return sendError({ res, message: 'Code already confirmed', LOGGER_PREFIX, code: 400 })
      }
      const codeFromDb = record.code
      const equalCodes = isEqualValues({ value1: code, value2: codeFromDb })
      if (!equalCodes) {
        return sendError({ res, message: 'Wrong code', LOGGER_PREFIX, code: 400 })
      }
      const token = generateToken(codeFromDb)
      await updateRecordByEmail({ db, email, key: 'token', value: token, table: 'codes' })
      await updateRecordByEmail({ db, email, key: 'is_active_token', value: true, table: 'codes' })
      await updateRecordByEmail({ db, email, key: 'is_confirmed_code', value: true, table: 'codes' })
      return sendResponse({ res, result: [null, token] })
    } catch (error) {
      return sendError({ res, message: error, LOGGER_PREFIX })
    }
  })

  passwordApi.post('/create-new-pass-by-token', validate(createPassByTokenValidation, validationOptions, {}), async (req, res) => {
    const { email, token, password } = req.body

    try {
      const result = await db.query('SELECT token, is_confirmed_code, is_active_token FROM codes WHERE email = ?', email)
      if (!result.length) {
        return sendError({ res, message: 'Wrong email', LOGGER_PREFIX, code: 400 })
      }
      const record = result[0]
      const isConfimedCode = record.is_confirmed_code
      if (!isConfimedCode) {
        return sendError({ res, message: 'Code did not confirm', LOGGER_PREFIX, code: 400 })
      }
      const isActiveToken = record.is_active_token
      if (!isActiveToken) {
        return sendError({ res, message: 'Token does not active', LOGGER_PREFIX, code: 400 })
      }
      const tokenFromDb = record.token
      const equalTokens = isEqualValues({ value1: token, value2: tokenFromDb })
      if (!equalTokens) {
        return sendError({ res, message: 'Wrong token', LOGGER_PREFIX, code: 400 })
      }
      const encryptedPassword = encryptPassword(password)
      await updateRecordByEmail({ db, email, key: 'password', value: encryptedPassword, table: 'users' })
      await updateRecordByEmail({ db, email, key: 'is_active_token', value: false, table: 'codes' })
      return sendResponse({ res, result: [null, true] })
    } catch (error) {
      return sendError({ res, message: error, LOGGER_PREFIX })
    }
  })

  passwordApi.post('/create-new-pass-by-pass', auth, validate(createPassByPassValidation, validationOptions, {}), async (req, res) => {
    const { user }: { user: User } = res.locals
    const { currentPassword, newPassword } = req.body

    try {
      const result = await db.query('SELECT password FROM users WHERE email = ?', user.email)
      if (!result.length) {
        return sendError({ res, message: 'Wrong email', LOGGER_PREFIX, code: 400 })
      }
      const record = result[0]
      const decryptedDbPassword = decryptPassword(record.password)
      const equalCurrentPasswords = isEqualValues({
        value1: currentPassword,
        value2: decryptedDbPassword,
      })
      if (!equalCurrentPasswords) {
        return sendError({ res, message: 'Wrong current passwrod', LOGGER_PREFIX, code: 400 })
      }
      const equalNewPassword = isEqualValues({ value1: newPassword, value2: decryptedDbPassword })
      if (equalNewPassword) {
        return sendError({ res, message: 'The new password must be different from the current one', LOGGER_PREFIX, code: 400 })
      }
      const encryptedNewPassword = encryptPassword(newPassword)
      await updateRecordByEmail({ db, email: user.email, key: 'password', value: encryptedNewPassword, table: 'users' })
      return sendResponse({ res, result: [null, true] })
    } catch (error) {
      return sendError({ res, message: error, LOGGER_PREFIX })
    }
  })

  return passwordApi
}
