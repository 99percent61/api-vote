import DB from '../../../lib/db'

async function savePinToDb(
  { db, code, email }: { db: DB, code: string, email: string },
): Promise<void> {
  const record = {
    email,
    code,
  }
  await db.query('INSERT into codes SET ?', record)
}

async function isSentCode({ db, email }: { db: DB, email: string }): Promise<boolean> {
  const result = await db.query('SELECT code FROM codes WHERE email = ?', email)
  return result.length > 0
}

function isEqualCodes({ clientCode, dbCode }: { clientCode: string, dbCode: string }): boolean {
  return String(clientCode) === String(dbCode)
}

function isEqualTokens(
  { clientToken, dbToken }: { clientToken: string, dbToken: string },
): boolean {
  return clientToken === dbToken
}

function isEqualValues({ value1, value2 }: { value1: string, value2: string }): boolean {
  return String(value1) === String(value2)
}

async function updateRecordByEmail(
  { db, key, value, email, table }
  : { db: DB, key: string, value: any, email: string, table: string },
): Promise<void> {
  await db.query(`UPDATE ${table} SET ${key} = ? WHERE email = ?`, [value, email])
}

export {
  savePinToDb,
  isSentCode,
  isEqualCodes,
  isEqualTokens,
  updateRecordByEmail,
  isEqualValues,
}
