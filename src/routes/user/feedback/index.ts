import { Router } from 'express'
import { IConfig } from 'config'
import { validate } from 'express-validation'
import { sendError, sendResponse } from '../../../lib/utils'
import Elasticsearch from '../../../lib/elasticsearch'
import DB from '../../../lib/db'
import Mailer from '../../../lib/mail'
import validationSchema from './validation'

export default ({ config, db }: { config?: IConfig, esClient?: Elasticsearch, db?: DB }) => {
  const feebackApi = Router()
  const LOGGER_PREFIX = 'FEEDBACK: '
  const mailer = new Mailer()
  const validationOptions = {
    context: true,
    keyByField: true,
  }

  feebackApi.post('/', validate(validationSchema, validationOptions, {}), async (req, res) => {
    const { title, message, email } = req.body

    try {
      mailer.createTransport()
      const html = await mailer.renderTemplate('feedback', { title, message, email })
      if (!html) {
        sendError({ res, message: 'Error rendering the email template', LOGGER_PREFIX })
        return
      }
      await mailer.send({ to: config.get('mailer.emailForFeedback'), subject: 'Feedback message', html })
      sendResponse({ res, result: [null, true] })
    } catch (error) {
      sendError({ res, message: error, LOGGER_PREFIX })
    }
  })

  return feebackApi
}
