import { Joi } from 'express-validation'
import { requiredString } from '../../../lib/validation'

export default {
  body: Joi.object({
    email: Joi.string().empty(''),
    title: requiredString,
    message: requiredString,
  }),
}
