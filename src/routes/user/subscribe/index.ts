import { Router } from 'express'
import { IConfig } from 'config'
import { validate } from 'express-validation'
import Elasticsearch from '../../../lib/elasticsearch'
import DB from '../../../lib/db'
import { sendResponse, sendError } from '../../../lib/utils'
import { auth } from '../../../middlewares'
import validationSchema from './validation'
import { User } from '../types'

export default ({ db }: { config?: IConfig, esClient?: Elasticsearch, db?: DB }) => {
  const subscribeApi = Router()
  const LOGGER_PREFIX = 'SUBSCRIBE: '
  const validationOptions = {
    context: true,
    keyByField: true,
  }

  subscribeApi.post('/', auth, validate(validationSchema, validationOptions), async (req, res) => {
    const { subscribeId }: { subscribeId: number } = req.body
    const { user }: { user: User } = res.locals

    try {
      const subscribeUser = await db.getUserById(subscribeId)
      if (!subscribeUser) {
        return sendError({ res, message: 'User for subscribe is not found', LOGGER_PREFIX })
      }

      const userSubscriptions = user.subscriptions || []
      await db.update({
        table: 'users',
        updateFields: ['subscriptions'],
        key: 'id',
        values: [JSON.stringify([...userSubscriptions, subscribeId]), user.id],
      })

      const userSubscribes = subscribeUser.subscribers || []
      await db.update({
        table: 'users',
        updateFields: ['subscribers'],
        key: 'id',
        values: [JSON.stringify([...userSubscribes, user.id]), subscribeId],
      })

      return sendResponse({ res, result: [null, true] })
    } catch (error) {
      return sendError({ res, message: error, LOGGER_PREFIX })
    }
  })

  return subscribeApi
}
