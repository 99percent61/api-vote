import { Router } from 'express'
import { IConfig } from 'config'
import { validate } from 'express-validation'

import DB from '../../../lib/db'
import Elasticsearch from '../../../lib/elasticsearch'
import { auth } from '../../../middlewares'
import { sendError, sendResponse } from '../../../lib/utils'
import validationSchema from './validation'
import { User } from '../types'

export default ({ db }: { config?: IConfig, esClient?: Elasticsearch, db?: DB }) => {
  const updateApi = Router()
  const LOGGER_PREFIX = 'UPDATE USER: '
  const validationOptions = {
    context: true,
    keyByField: true,
  }

  updateApi.put('/', auth, validate(validationSchema, validationOptions), async (req, res) => {
    const { user }: { user: User } = res.locals
    const dataForUpdate = req.body

    if (!user || !user.id) {
      return sendError({ res, message: 'User does not exist', code: 403 })
    }

    try {
      await db.update({
        table: 'users',
        updateFields: Object.keys(dataForUpdate),
        key: 'id',
        values: [...Object.values(dataForUpdate), user.id],
      })
      return sendResponse({ res, result: [null, true] })
    } catch (error) {
      return sendError({ res, message: error, LOGGER_PREFIX })
    }
  })

  return updateApi
}
