import { Joi } from 'express-validation'
import { string } from '../../../lib/validation'

export default {
  body: Joi.object({
    avatar: string,
  }),
}
