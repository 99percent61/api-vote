import { Router } from 'express'
import { IConfig } from 'config'
import pick from 'lodash/pick'
import DB from '../../../lib/db'
import Elasticsearch from '../../../lib/elasticsearch'
import { auth } from '../../../middlewares'
import { User } from '../types'
import { sendError, sendResponse } from '../../../lib/utils'

export default ({ db }: { config?: IConfig, esClient?: Elasticsearch, db?: DB }) => {
  const termsApi = Router()
  const LOGGER_PREFIX = 'USER TERMS: '

  termsApi.put('/', auth, async (req, res) => {
    const { user }: { user: User } = res.locals

    try {
      await db.query('UPDATE users SET terms = ? WHERE id = ?', [1, user.id])
      const dbUser = await db.getUserById(user.id)
      const preparedUser = pick(
        dbUser,
        ['age', 'avatar', 'black_list', 'email', 'id', 'is_blocked', 'login', 'name', 'settings', 'sex', 'subscribers', 'subscriptions', 'terms'],
      )
      return sendResponse({ res, result: [null, preparedUser] })
    } catch (error) {
      return sendError({ res, message: error, LOGGER_PREFIX })
    }
  })

  return termsApi
}
