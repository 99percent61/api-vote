import { Joi } from 'express-validation'
import { login, password, email, name, sex, age } from '../../../lib/validation'

export default {
  body: Joi.object({
    login,
    email,
    name,
    sex,
    age,
    password,
    repeatedPassword: Joi.ref('password'),
  }).with('password', 'repeatedPassword'),
}
