import logger from '../../../lib/logger'
import DB from '../../../lib/db'
import { User } from '../types'
import { encryptPassword } from '../../../lib/auth'

const LOGGER_PREFIX = 'REGISTRATION USER: '

async function registrationUser(
  { user, db }: { user: User, db: DB },
): Promise<[any, number | null]> {
  try {
    const dbLogin = await db.query(`SELECT login FROM users WHERE login = '${user.login}'`)
    const dbEmail = await db.query(`SELECT email FROM users WHERE email = '${user.email}'`)

    if (dbLogin.length) {
      return ['Login busy', null]
    }
    if (dbEmail.length) {
      return ['Email busy', null]
    }

    const encryptedPassword = encryptPassword(user.password)
    Object.assign(user, { is_blocked: false, password: encryptedPassword, terms: 1 })
    const record = db.prepareRecord(user)
    const { insertId } = await db.query('INSERT into users SET ?', record)
    return [null, insertId]
  } catch (error) {
    logger.error(LOGGER_PREFIX, error)
    return [error, null]
  }
}

export {
  registrationUser,
}
