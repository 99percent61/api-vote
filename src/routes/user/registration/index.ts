import { Router } from 'express'
import { validate } from 'express-validation'
import pick from 'lodash/pick'
import { IConfig } from 'config'
import validationSchema from './validation'
import DB from '../../../lib/db'
import Elasticsearch from '../../../lib/elasticsearch'
import { registrationUser } from './helpers'
import { sendError, sendResponse } from '../../../lib/utils'
import { generateToken } from '../../../lib/auth'
import { AlertService } from '../../../services'

export default ({ db }: { config?: IConfig, esClient?: Elasticsearch, db?: DB }) => {
  const registartionApi = Router()
  const LOGGER_PREFIX = 'REGISTRATIONS USER: '
  const validationOptions = {
    context: true,
    keyByField: true,
  }

  registartionApi.post('/', validate(validationSchema, validationOptions, {}), async (req, res) => {
    const user = pick(req.body, ['login', 'name', 'email', 'sex', 'password', 'age'])
    const [error, userId] = await registrationUser({ user, db })
    if (error) {
      return sendError({ res, message: error, LOGGER_PREFIX })
    }
    await AlertService.sendMessage(`Congrats!!! New registered user with id ${userId}`)
    const token = generateToken(userId)
    const headers = { key: 'x-auth-token', value: token }
    return sendResponse({ res, result: [null, userId], headers })
  })

  return registartionApi
}
