import { Router } from 'express'
import { IConfig } from 'config'
import { validate } from 'express-validation'
import Elasticsearch from '../../../lib/elasticsearch'
import DB from '../../../lib/db'
import { auth } from '../../../middlewares'
import { sendResponse, sendError } from '../../../lib/utils'
import validationSchema from './validation'
import { User } from '../types'

export default ({ db }: { config?: IConfig, esClient?: Elasticsearch, db?: DB }) => {
  const unSubscribeApi = Router()
  const LOGGER_PREFIX = 'UNSUBSCRIBE: '
  const validationOptions = {
    context: true,
    keyByField: true,
  }

  unSubscribeApi.post('/', auth, validate(validationSchema, validationOptions), async (req, res) => {
    const { unsubscribeId }: { unsubscribeId: number } = req.body
    const { user }: { user: User } = res.locals

    try {
      const unsubscribeUser = await db.getUserById(unsubscribeId)
      if (!unsubscribeUser) {
        return sendError({ res, message: 'User for unsubscribe is not found', LOGGER_PREFIX })
      }

      const userSubscriptions = (user.subscriptions || [])
        .filter((id) => Number(id) !== Number(unsubscribeId))
      await db.update({
        table: 'users',
        updateFields: ['subscriptions'],
        key: 'id',
        values: [JSON.stringify(userSubscriptions), user.id],
      })

      const userSubscribes = (unsubscribeUser.subscribers || [])
        .filter((id) => Number(id) !== Number(user.id))
      await db.update({
        table: 'users',
        updateFields: ['subscribers'],
        key: 'id',
        values: [JSON.stringify(userSubscribes), unsubscribeId],
      })

      return sendResponse({ res, result: [null, true] })
    } catch (error) {
      return sendError({ res, message: error, LOGGER_PREFIX })
    }
  })

  return unSubscribeApi
}
