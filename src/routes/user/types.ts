type blackListItem = {
  userId: number
}

export interface User {
  id?: number
  login?: string
  email?: string
  name?: string
  sex?: string
  password?: string
  settings?: Record<string, any>
  avatar?: string
  black_list?: blackListItem[]
  is_blocked?: boolean
  age?: number
  subscribers?: number[],
  subscriptions?: number[],
  terms?: 0 | 1
}
