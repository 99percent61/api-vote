import { Joi } from 'express-validation'
import { login, password } from '../../../lib/validation'

export default {
  body: Joi.object({
    login,
    password,
  }),
}
