import { Router } from 'express'
import { IConfig } from 'config'
import { validate } from 'express-validation'
import DB from '../../../lib/db'
import Elasticsearch from '../../../lib/elasticsearch'
import validationSchema from './validation'
import { sendError, sendResponse } from '../../../lib/utils'
import { User } from '../types'
import { decryptPassword, isEqualPasswords, generateToken } from '../../../lib/auth'

export default ({ db }: { config?: IConfig, esClient?: Elasticsearch, db?: DB }) => {
  const sessionApi = Router()
  const LOGGER_PREFIX = 'USER SESSION: '
  const validationOptions = {
    context: true,
    keyByField: true,
  }

  sessionApi.post('/create', validate(validationSchema, validationOptions, {}), async (req, res) => {
    const { login, password }: { login: string, password: string } = req.body

    try {
      const result = await db.query('SELECT id, password FROM users WHERE login = ?', login)
      if (!result.length) {
        return sendError({ res, message: 'User not found', LOGGER_PREFIX, code: 404 })
      }

      const user: User = result[0]
      const decryptedPassword = decryptPassword(user.password)
      const equalPasswords = isEqualPasswords({
        clientPassword: password,
        dbPassword: decryptedPassword,
      })
      if (!equalPasswords) {
        return sendError({ res, message: 'Forbidden. Wrong password', code: 403, LOGGER_PREFIX })
      }
      const token = generateToken(user.id)
      const headers = { key: 'x-auth-token', value: token }
      return sendResponse({ res, result: [null, user.id], headers })
    } catch (error) {
      return sendError({ res, message: error, LOGGER_PREFIX })
    }
  })

  return sessionApi
}
