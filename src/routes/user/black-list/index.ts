import { Router } from 'express'
import { IConfig } from 'config'
import { validate } from 'express-validation'
import Elasticsearch from '../../../lib/elasticsearch'
import DB from '../../../lib/db'
import { sendResponse, sendError } from '../../../lib/utils'
import { auth } from '../../../middlewares'
import validationSchema from './validation'
import { User } from '../types'

export default ({ db }: { config?: IConfig, esClient?: Elasticsearch, db?: DB }) => {
  const blackListApi = Router()
  const LOGGER_PREFIX = 'BLACK LIST: '
  const validationOptions = {
    context: true,
    keyByField: true,
  }

  blackListApi.post('/add', auth, validate(validationSchema, validationOptions), async (req, res) => {
    const userIdToBlackList = req.body.userId
    const { user }: { user: User } = res.locals

    try {
      const userBlackList = user.black_list || []
      await db.update({
        table: 'users',
        updateFields: ['black_list'],
        key: 'id',
        values: [JSON.stringify([...userBlackList, userIdToBlackList]), user.id],
      })

      return sendResponse({ res, result: [null, true] })
    } catch (error) {
      return sendError({ res, message: error.message, LOGGER_PREFIX })
    }
  })

  blackListApi.post('/remove', auth, validate(validationSchema, validationOptions), async (req, res) => {
    const userIdToRemoveFromBlackList = req.body.userId
    const { user }: { user: User } = res.locals

    try {
      const userBlackList = (user.black_list || [])
        .filter((id) => Number(id) !== Number(userIdToRemoveFromBlackList))
      await db.update({
        table: 'users',
        updateFields: ['black_list'],
        key: 'id',
        values: [JSON.stringify(userBlackList), user.id],
      })

      return sendResponse({ res, result: [null, true] })
    } catch (error) {
      return sendError({ res, message: error.message, LOGGER_PREFIX })
    }
  })

  return blackListApi
}
