import { Router } from 'express'
import { IConfig } from 'config'
import pick from 'lodash/pick'
import DB from '../../../lib/db'
import Elasticsearch from '../../../lib/elasticsearch'
import { sendError, sendResponse } from '../../../lib/utils'
import { auth } from '../../../middlewares'

export default ({ db }: { config?: IConfig, esClient?: Elasticsearch, db?: DB }) => {
  const meApi = Router()
  const LOGGER_PREFIX = 'USER ME: '

  meApi.get('/', auth, async (req, res) => {
    const { user } = res.locals

    try {
      const preparedUser = pick(
        user,
        ['age', 'avatar', 'black_list', 'email', 'id', 'is_blocked', 'login', 'name', 'settings', 'sex', 'subscribers', 'subscriptions', 'terms'],
      )
      return sendResponse({ res, result: [null, preparedUser] })
    } catch (error) {
      return sendError({ res, message: error, LOGGER_PREFIX })
    }
  })

  return meApi
}
