import { Router } from 'express'
import registration from './registration'
import me from './me'
import session from './session'
import password from './password'
import subscribe from './subscribe'
import unsubscribe from './unsubscribe'
import blackList from './black-list'
import update from './update'
import feedback from './feedback'
import terms from './terms'

export default ({ config, esClient, db }) => {
  const userApi = Router()

  userApi.use('/registration', registration({ config, esClient, db }))
  userApi.use('/me', me({ config, esClient, db }))
  userApi.use('/session', session({ config, esClient, db }))
  userApi.use('/password', password({ config, esClient, db }))
  userApi.use('/subscribe', subscribe({ config, esClient, db }))
  userApi.use('/unsubscribe', unsubscribe({ config, esClient, db }))
  userApi.use('/black-list', blackList({ config, esClient, db }))
  userApi.use('/update', update({ config, esClient, db }))
  userApi.use('/feedback', feedback({ config, esClient, db }))
  userApi.use('/terms', terms({ config, esClient, db }))

  return userApi
}
