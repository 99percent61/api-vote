import { Router } from 'express'
import { IConfig } from 'config'
import { validate } from 'express-validation'
import dayjs from 'dayjs'
import Elasticsearch from '../../../lib/elasticsearch'
import DB from '../../../lib/db'
import { auth } from '../../../middlewares'
import { sendError, sendResponse } from '../../../lib/utils'
import validationSchema from './validation'
import { Complaint, ComplaintTypes } from '../types'
import { createComplaint, sendEmail } from './helpers'

type Body = {
  type: string,
  poll_id: number
}

export default ({ db, esClient }: { config?: IConfig, esClient?: Elasticsearch, db?: DB }) => {
  const createComplaintApi = Router()
  const LOGGER_PREFIX = 'COMPLAINT CREATE: '
  const validationOptions = {
    context: true,
    keyByField: true,
  }

  createComplaintApi.put('/', auth, validate(validationSchema, validationOptions), async (req, res) => {
    const { type, poll_id }: Body = req.body

    const isValidIncomingType = Object.values(ComplaintTypes).some((value) => value === type)

    if (!isValidIncomingType) {
      return sendError({ res, message: `Incoming type isn't valid value - ${type}`, LOGGER_PREFIX, code: 400 })
    }

    const hasPoll = await db.getPollById(poll_id)
    if (!hasPoll) {
      return sendError({ res, message: `Poll with entered id doesn't exist - ${poll_id}`, LOGGER_PREFIX, code: 404 })
    }

    const complaint: Complaint = {
      poll_id,
      type,
      created_at: dayjs().format('YYYY-MM-DDTHH:mm:ssZ[Z]'),
    }

    try {
      const insertId = await createComplaint({ complaint, db, esClient })
      await sendEmail({ pollId: poll_id, type })
      return sendResponse({ res, result: [null, insertId] })
    } catch (error) {
      return sendError({ res, message: error, LOGGER_PREFIX })
    }
  })

  return createComplaintApi
}
