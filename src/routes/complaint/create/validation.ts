import { Joi } from 'express-validation'
import { requiredString, requiredInteger } from '../../../lib/validation'

export default {
  body: Joi.object({
    poll_id: requiredInteger,
    type: requiredString,
  }),
}
