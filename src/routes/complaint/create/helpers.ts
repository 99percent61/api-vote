import config from 'config'
import DB from '../../../lib/db'
import Elasticsearch from '../../../lib/elasticsearch'
import { Complaint } from '../types'
import Mailer from '../../../lib/mail'

async function createComplaint(
  { complaint, db, esClient }
  : { complaint: Complaint, db: DB, esClient: Elasticsearch },
) {
  const record = db.prepareRecord(complaint)
  const { insertId } = await db.query('INSERT into complaints SET ?', record)
  const dbComplaint = await db.getComplaintById(insertId)
  await esClient.createDocument({
    document: dbComplaint,
    indexName: config.get('elasticsearch.indices.complaint'),
    entityType: config.get('elasticsearch.types.complaint'),
  })
  return insertId
}

async function sendEmail({ pollId, type }: { pollId: number, type: string }): Promise<void> {
  const mailer = new Mailer()
  mailer.createTransport()
  const html = await mailer.renderTemplate('complaint', { pollId, type })
  if (!html) {
    throw new Error('Error rendering the email template')
  }
  await mailer.send({ to: config.get('mailer.emailForFeedback'), subject: 'Feedback message', html })
}

export {
  createComplaint,
  sendEmail,
}
