export enum ComplaintTypes {
  Spam = 'spam',
  Harassment = 'harassment',
  AdultContent = 'adultContent',
  ChildPornoghraphy = 'childPornography',
  DrugAdvocacy = 'drugAdvocacy',
  Weapon = 'weapon',
  Violance = 'violence',
  Bullying = 'bullying',
  Suicide = 'suicide',
  AnimalAbuse = 'animalAbuse',
  MisleadingInformation = 'misleadingInformation',
  Fraud = 'fraud',
  Extremism = 'extremism',
  HateSpeech = 'hateSpeech',
  Other = 'other'
}

export type Complaint = {
  id?: number
  poll_id: number
  type: string
  created_at: string
}
