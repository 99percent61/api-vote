import { Router } from 'express'
import createRoute from './create'

export default ({ config, esClient, db }) => {
  const pollApi = Router()

  pollApi.use('/create', createRoute({ config, esClient, db }))

  return pollApi
}
