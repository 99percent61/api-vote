import { Router } from 'express'
import { IConfig } from 'config'
import { validate } from 'express-validation'
import dayjs from 'dayjs'
import Elasticsearch from '../../../lib/elasticsearch'
import DB from '../../../lib/db'
import { auth } from '../../../middlewares'
import { sendError, sendResponse } from '../../../lib/utils'
import validationSchema from './validation'
import { Poll } from '../types'
import { User } from '../../user/types'
import { createPoll } from './helpers'

export default ({ db, esClient }: { config?: IConfig, esClient?: Elasticsearch, db?: DB }) => {
  const createPollApi = Router()
  const LOGGER_PREFIX = 'POLL CREATE: '
  const validationOptions = {
    context: true,
    keyByField: true,
  }

  createPollApi.put('/', auth, validate(validationSchema, validationOptions), async (req, res) => {
    const { user }: { user: User } = res.locals

    if (!user.terms) {
      return sendError({ res, message: "User didn't accept terms and conditions", LOGGER_PREFIX })
    }

    const poll: Poll = req.body
    const currentTime = new Date()
    const expire = new Date(currentTime.setHours(currentTime.getHours() + poll.time_limit))
      .toISOString()
    Object.assign(poll, {
      created_at: dayjs().format('YYYY-MM-DDTHH:mm:ssZ[Z]'),
      updated_at: dayjs().format('YYYY-MM-DDTHH:mm:ssZ[Z]'),
      expire_at: dayjs(expire).format('YYYY-MM-DDTHH:mm:ssZ[Z]'),
      is_expired: false,
      is_blocked: false,
      author: JSON.stringify({
        id: user.id,
        name: user.name || '',
        login: user.login,
        avatar: user.avatar,
      }),
      answers: JSON.stringify(poll.answers),
      image: JSON.stringify(poll.image),
    })

    try {
      const insertId = await createPoll({ poll, db, esClient })
      return sendResponse({ res, result: [null, insertId] })
    } catch (error) {
      return sendError({ res, message: error, LOGGER_PREFIX })
    }
  })

  return createPollApi
}
