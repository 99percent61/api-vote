import { Joi } from 'express-validation'
import { requiredString, answers, interger, image, string } from '../../../lib/validation'

export default {
  body: Joi.object({
    answers,
    image,
    audio: string,
    video: string,
    title: requiredString,
    time_limit: interger,
  }),
}
