import config from 'config'
import { Poll } from '../types'
import DB from '../../../lib/db'
import Elasticsearch from '../../../lib/elasticsearch'

async function createPoll({ poll, db, esClient }: { poll: Poll, db: DB, esClient: Elasticsearch }) {
  const record = db.prepareRecord(poll)
  const { insertId } = await db.query('INSERT into polls SET ?', record)
  const dbPoll = await db.getPollById(insertId)
  await esClient.createDocument({
    document: dbPoll,
    indexName: config.get('elasticsearch.indices.poll'),
    entityType: config.get('elasticsearch.types.poll'),
  })
  return insertId
}

export {
  createPoll,
}
