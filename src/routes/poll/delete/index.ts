import { Router } from 'express'
import { IConfig } from 'config'
import { validate } from 'express-validation'
import Elasticsearch from '../../../lib/elasticsearch'
import DB from '../../../lib/db'
import { sendError, sendResponse } from '../../../lib/utils'
import { auth } from '../../../middlewares'
import { User } from '../../user/types'
import validationSchema from './validation'
import { deletePoll } from '../helpers'

export default ({ db, esClient }: { config?: IConfig, esClient?: Elasticsearch, db?: DB }) => {
  const deletePollApi = Router()
  const LOGGER_PREFIX = 'POLL DELETE: '
  const validationOptions = {
    context: true,
    keyByField: true,
  }

  deletePollApi.delete('/', auth, validate(validationSchema, validationOptions), async (req, res) => {
    const { id } = req.body
    const { user }: { user: User } = res.locals
    try {
      const dbPoll = await db.getPollById(id)
      if (!dbPoll) {
        return sendError({ res, message: 'Poll does not exist', code: 400, LOGGER_PREFIX })
      }
      if (Number(dbPoll.author.id) !== Number(user.id)) {
        return sendError({ res, message: 'User does not assign to poll', code: 403 })
      }
      await deletePoll({ id, db, esClient })
      return sendResponse({ res, result: [null, true] })
    } catch (error) {
      return sendError({ res, message: error, LOGGER_PREFIX })
    }
  })

  return deletePollApi
}
