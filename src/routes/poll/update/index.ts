import { Router } from 'express'
import { IConfig } from 'config'
import { validate } from 'express-validation'
import dayjs from 'dayjs'
import Elasticsearch from '../../../lib/elasticsearch'
import DB from '../../../lib/db'
import { auth } from '../../../middlewares'
import { User } from '../../user/types'
import { sendError, sendResponse } from '../../../lib/utils'
import validationSchema from './validation'
import { Poll } from '../types'
import { updatePoll } from '../helpers'

export default ({ db, esClient }: { config?: IConfig, esClient?: Elasticsearch, db?: DB }) => {
  const updatePollApi = Router()
  const LOGGER_PREFIX = 'POLL UPDATE: '
  const validationOptions = {
    context: true,
    keyByField: true,
  }

  updatePollApi.put('/', auth, validate(validationSchema, validationOptions), async (req, res) => {
    const poll: Poll = req.body
    const { user }: { user: User } = res.locals

    if (!user.terms) {
      return sendError({ res, message: "User didn't accept terms and conditions", LOGGER_PREFIX })
    }

    try {
      const dbPoll = await db.getPollById(poll.id)
      if (!dbPoll) {
        return sendError({ res, message: 'Poll does not exist', code: 400, LOGGER_PREFIX })
      }
      if (dbPoll.is_expired) {
        return sendError({ res, message: 'Poll is expired', code: 400, LOGGER_PREFIX })
      }
      if (Number(dbPoll.author.id) !== Number(user.id)) {
        return sendError({ res, message: 'User does not assign to poll', code: 403 })
      }

      const updatedPoll = {
        id: poll.id,
        is_expired: poll.is_expired,
        updated_at: dayjs().format('YYYY-MM-DDTHH:mm:ssZ[Z]'),
      }

      await updatePoll({ poll: updatedPoll, db, esClient })
      return sendResponse({ res, result: [null, true] })
    } catch (error) {
      return sendError({ res, message: error, LOGGER_PREFIX })
    }
  })

  return updatePollApi
}
