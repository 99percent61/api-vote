import { Joi } from 'express-validation'
import { requiredInteger, interger } from '../../../lib/validation'

export default {
  body: Joi.object({
    id: requiredInteger,
    is_expired: interger,
  }),
}
