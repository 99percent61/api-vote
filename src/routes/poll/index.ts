import { Router } from 'express'
import createRoute from './create'
import updateRoute from './update'
import deleteRoute from './delete'
import listRoute from './list'
import vote from './vote'
import statistics from './statistics'

export default ({ config, esClient, db }) => {
  const pollApi = Router()

  pollApi.use('/create', createRoute({ config, esClient, db }))
  pollApi.use('/update', updateRoute({ config, esClient, db }))
  pollApi.use('/delete', deleteRoute({ config, esClient, db }))
  pollApi.use('/list', listRoute({ config, esClient, db }))
  pollApi.use('/vote', vote({ config, esClient, db }))
  pollApi.use('/statistics', statistics({ config, esClient, db }))

  return pollApi
}
