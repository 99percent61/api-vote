export type Answer = {
  title: string
  votes: number
  id: number
}

export interface Poll {
  id?: number
  title?: string
  author?: {
    id: number
    name: string
    login: string
    avatar: string
  }
  created_at?: string
  updated_at?: string
  expire_at?: string
  is_expired?: boolean
  is_blocked?: boolean
  time_limit?: number
  answers?: Answer[] | string
  image?: string | string[]
  video?: string
  audio?: string
  user_vote?: number | null
}

export interface Vote {
  id?: number,
  user_id: number,
  poll_id: number,
  answer_id: number
}
