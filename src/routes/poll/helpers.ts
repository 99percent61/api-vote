import omit from 'lodash/omit'
import config from 'config'
import { Poll } from './types'
import DB from '../../lib/db'
import Elasticsearch from '../../lib/elasticsearch'

async function updatePoll(
  { poll, db, esClient }: { poll: Poll, db: DB, esClient: Elasticsearch },
): Promise<void> {
  const { id } = poll
  const record = db.prepareRecord(omit(poll, [id]))
  await db.update({ table: 'polls', updateFields: Object.keys(record), key: 'id', values: [...Object.values(record), id] })
  const dbPoll = await db.getPollById(poll.id)
  await esClient.update({
    id: poll.id,
    index: config.get('elasticsearch.indices.poll'),
    type: config.get('elasticsearch.types.poll'),
    refresh: true,
    body: {
      doc: dbPoll,
    },
  })
}

async function deletePoll(
  { id, db, esClient }: { id: string | number, db: DB, esClient: Elasticsearch },
): Promise<void> {
  await db.delete({ table: 'polls', key: 'id', value: id })
  await esClient.delete({
    id,
    index: config.get('elasticsearch.indices.poll'),
    type: config.get('elasticsearch.types.poll'),
    refresh: true,
  })
}

export {
  updatePoll,
  deletePoll,
}
