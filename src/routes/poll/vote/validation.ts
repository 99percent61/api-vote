import { Joi } from 'express-validation'
import { requiredInteger } from '../../../lib/validation'

export default {
  body: Joi.object({
    poll_id: requiredInteger,
    answer_id: requiredInteger,
  }),
}
