import { IConfig } from 'config'

import DB from '../../../lib/db'
import Elasticsearch from '../../../lib/elasticsearch'
import { Vote } from '../types'

async function createVote(
  { vote, esClient, config, db }: { vote: Vote, esClient: Elasticsearch, config: IConfig, db: DB },
): Promise<number> {
  const newVote = await db.query('INSERT into votes SET ?', vote)
  const voteId: number = newVote.insertId
  await esClient.createDocument({
    document: { ...vote, id: voteId },
    indexName: config.get('elasticsearch.indices.vote'),
    entityType: config.get('elasticsearch.types.vote'),
  })
  return voteId
}

export {
  createVote,
}
