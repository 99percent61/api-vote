import { Router } from 'express'
import { IConfig } from 'config'
import { validate } from 'express-validation'
import dayjs from 'dayjs'
import Elasticsearch from '../../../lib/elasticsearch'
import DB from '../../../lib/db'
import { auth } from '../../../middlewares'
import { User } from '../../user/types'
import { sendError, sendResponse } from '../../../lib/utils'
import validationSchema from './validation'
import { createVote } from './helpers'

export default (
  { db, esClient, config }: { config?: IConfig, esClient?: Elasticsearch, db?: DB },
) => {
  const votePollApi = Router()
  const LOGGER_PREFIX = 'POLL VOTE: '
  const validationOptions = {
    context: true,
    keyByField: true,
  }

  votePollApi.put('/', auth, validate(validationSchema, validationOptions), async (req, res) => {
    const { poll_id, answer_id } = req.body
    const { user }: { user: User } = res.locals

    try {
      const dbPoll = await db.getPollById(poll_id)
      if (!dbPoll) {
        return sendError({ res, message: 'Poll does not exist', code: 400, LOGGER_PREFIX })
      }
      if (dbPoll.is_expired) {
        return sendError({ res, message: 'Poll is expired', code: 400, LOGGER_PREFIX })
      }

      const dbVote = await db.query('SELECT id FROM votes WHERE user_id = ? AND poll_id = ?', [user.id, poll_id])

      if (dbVote.length) {
        return sendError({ res, message: 'User has already voted', code: 400, LOGGER_PREFIX })
      }

      const vote = {
        user_id: user.id,
        poll_id,
        answer_id,
        created_at: dayjs().format('YYYY-MM-DDTHH:mm:ssZ[Z]'),
      }

      const voteId = await createVote({ vote, esClient, db, config })

      return sendResponse({ res, result: [null, voteId] })
    } catch (error) {
      return sendError({ res, message: error, LOGGER_PREFIX })
    }
  })

  return votePollApi
}
