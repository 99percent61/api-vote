import bodybuilder from 'bodybuilder'
import { User } from '../../user/types'
import { Vote } from '../types'
import DB from '../../../lib/db'

function buildQueryForVoteList({ pollId }: { pollId: number }): Record<string, any> {
  const query = bodybuilder().query('term', 'poll_id', pollId)
  return query.build()
}

function extractUsersId(votes: Vote[]): number[] {
  return votes.map((vote) => vote.user_id)
}

async function getUsersFromBd({ userIds, db }: { userIds: number[], db: DB }): Promise<User[]> {
  const users = db.query(`SELECT id, sex, age FROM users WHERE id IN (${userIds.join(',')})`)
  return users;
}

function getAgeRange(age: number): string {
  const firsNum = Number(String(age).charAt(0))
  if (firsNum === 1) {
    return '<20'
  } if (firsNum >= 6) {
    return '>60'
  }
  return `${firsNum}0-${firsNum + 1}0`
}

function countVotes({ votes, users }: { votes: Vote[], users: User[] }) {
  const params = ['age', 'gender']
  const result = {}
  for (const vote of votes) {
    const answerId = vote.answer_id
    const user = users.find((item) => item.id === vote.user_id)
    if (!user) continue

    if (!result[answerId]) {
      result[answerId] = {}
    }
    params.forEach((param) => {
      if (!result[answerId][param]) {
        result[answerId][param] = {}
      }

      switch (param) {
        case 'age':
          const ageRange = getAgeRange(user.age)
          if (!result[answerId][param][ageRange]) {
            result[answerId][param][ageRange] = 0
          }

          result[answerId][param][ageRange] += 1
          break
        case 'gender':
          const gender = user.sex
          if (!result[answerId][param][gender]) {
            result[answerId][param][gender] = 0
          }

          result[answerId][param][gender] += 1
          break
        default:
          break
      }
    })
  }

  return result
}

export {
  buildQueryForVoteList,
  extractUsersId,
  getUsersFromBd,
  countVotes,
}
