import { Joi } from 'express-validation'
import { requiredInteger } from '../../../lib/validation'

export default {
  query: Joi.object({
    pollId: requiredInteger,
  }),
}
