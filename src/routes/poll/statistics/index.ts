import { Router } from 'express'
import { IConfig } from 'config'
import { validate } from 'express-validation'
import Elasticsearch from '../../../lib/elasticsearch'
import DB from '../../../lib/db'
import { auth } from '../../../middlewares'
import { sendError, sendResponse } from '../../../lib/utils'
import validationSchema from './validation'
import {
  buildQueryForVoteList,
  extractUsersId,
  getUsersFromBd,
  countVotes,
} from './helpers'
import { User } from '../../user/types'
import { Vote } from '../types'

export default (
  { db, esClient, config }: { config?: IConfig, esClient?: Elasticsearch, db?: DB },
) => {
  const statisticsPollApi = Router()
  const LOGGER_PREFIX = 'POLL STATISTICS: '
  const validationOptions = {
    context: true,
    keyByField: true,
  }

  statisticsPollApi.get('/list-by-poll', auth, validate(validationSchema, validationOptions), async (req, res) => {
    const { pollId } = req.query

    try {
      const query = buildQueryForVoteList({ pollId: Number(pollId) })
      const response = await esClient.search({
        index: config.get('elasticsearch.indices.vote'),
        type: config.get('elasticsearch.types.vote'),
        body: query,
        from: 0,
        size: 10000,
      })
      const votes: Vote[] = esClient.extractSourceFromHits(response.hits.hits)
      if (votes.length) {
        const userIds: number[] = extractUsersId(votes)
        const users: User[] = await getUsersFromBd({ userIds, db })
        const countedVotes = countVotes({ votes, users })

        return sendResponse({ res, result: [null, countedVotes], code: 200 })
      }

      return sendResponse({ res, result: [null, null], code: 200 })
    } catch (error) {
      return sendError({ res, message: error, LOGGER_PREFIX })
    }
  })

  return statisticsPollApi
}
