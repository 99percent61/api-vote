import bodybuilder from 'bodybuilder'
import { IConfig } from 'config'

import Elasticsearch, { Bucket } from '../../../lib/elasticsearch'
import { Poll, Vote, Answer } from '../types'

function assignSearchAfter({ query, searchAfter }: { query: any, searchAfter: string }): object {
  return {
    ...query.build(),
    search_after: [searchAfter],
  }
}

function buildQueryForList(
  { subscriptions, searchAfter, isExpired = '0' }: { subscriptions: number[], searchAfter: string, isExpired?: string },
): Record<string, any> {
  let query = bodybuilder()
  if (subscriptions && subscriptions.length) {
    query = query.query('terms', 'author.id', subscriptions)
  }
  query = query.filter('term', 'is_expired', isExpired)
  query = query.filter('term', 'is_blocked', 0)

  return assignSearchAfter({ query, searchAfter })
}

function buildQueryForOwnList(
  { userId, searchAfter, isExpired }: { userId: number, searchAfter: string, isExpired?: string },
): Record<string, any> {
  let query = bodybuilder().query('term', 'author.id', userId)
  if (isExpired !== undefined) {
    query = query.filter('term', 'is_expired', isExpired)
  }
  return assignSearchAfter({ query, searchAfter })
}

function buildQueryForSingle(
  { userId, pollId }: { userId: number, pollId: string },
): Record<string, any> {
  let query = bodybuilder().query('term', 'id', pollId)
  if (userId) {
    query = query.filter('term', 'author.id', userId)
  }
  return query.build()
}

function buildQueryForVoteList(
  { userId, pollIds }: { userId: number, pollIds: number[] },
): Record<string, any> {
  const query = bodybuilder()
    .query('term', 'user_id', userId)
    .filter('terms', 'poll_id', pollIds)
    .build()
  return query
}

function buildQueryForVotes(
  { pollIds }: { pollIds: number[] },
): Record<string, any> {
  let query = bodybuilder().query('terms', 'poll_id', pollIds)
  pollIds.forEach((pollId) => {
    query = query.aggregation('filter', { term: { poll_id: pollId } }, String(pollId), (a) => (
      a.aggregation('terms', 'answer_id', 'votes')
    ))
  })
  return query
    .size(0)
    .build()
}

function getSort(sort: string): any[] {
  const splittedSort = sort.split(':')
  if (splittedSort.length > 1) {
    return [
      {
        [splittedSort[0]]: splittedSort[1],
      },
    ]
  }
  return [sort]
}

async function assignUserVotes(
  {
    polls,
    esClient,
    config,
    userId,
  }: { polls: Poll[], esClient: Elasticsearch, config: IConfig, userId: number },
): Promise<void> {
  const pollIds: number[] = polls.map((poll) => poll.id)
  const query = buildQueryForVoteList({ userId, pollIds })
  const response = await esClient.search({
    index: config.get('elasticsearch.indices.vote'),
    type: config.get('elasticsearch.types.vote'),
    body: query,
    size: 20,
  })
  response.hits.hits = esClient.extractSourceFromHits(response.hits.hits)
  const votes: Vote[] = response.hits.hits

  polls.forEach((poll) => {
    poll.user_vote = null
    const vote = votes.find((item) => item.user_id === userId && item.poll_id === poll.id)
    if (vote !== undefined) {
      poll.user_vote = vote.answer_id
    }
  })
}

async function assignVotes(
  {
    polls,
    esClient,
    config,
  }: { polls: Poll[], esClient: Elasticsearch, config: IConfig },
): Promise<void> {
  const pollIds: number[] = polls.map((poll) => poll.id)
  const query = buildQueryForVotes({ pollIds })
  const response = await esClient.search({
    index: config.get('elasticsearch.indices.vote'),
    type: config.get('elasticsearch.types.vote'),
    body: query,
  })
  response.hits.hits = esClient.extractSourceFromHits(response.hits.hits)
  const { aggregations } = response

  polls.forEach((poll) => {
    const aggs = aggregations[poll.id]
    if (aggs !== undefined) {
      const { buckets }: { buckets: Bucket[] } = aggs.votes
      const answers = poll.answers as Answer[]
      answers.forEach((answer) => {
        const voteAggs = buckets.find((bucket) => bucket.key === answer.id)
        if (voteAggs !== undefined) {
          answer.votes = voteAggs.doc_count
        }
      })
    }
  })
}

export {
  buildQueryForList,
  buildQueryForOwnList,
  buildQueryForVoteList,
  buildQueryForSingle,
  assignUserVotes,
  assignVotes,
  getSort,
}
