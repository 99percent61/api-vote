import { Router } from 'express'
import { IConfig } from 'config'
import { validate } from 'express-validation'
import Elasticsearch from '../../../lib/elasticsearch'
import DB from '../../../lib/db'
import { User } from '../../user/types'
import { auth } from '../../../middlewares'
import { buildQueryForList, buildQueryForOwnList, getSort, assignUserVotes, assignVotes, buildQueryForSingle } from './helpers'
import { listValidation, ownListValidation, ownSingleValidation } from './validation'
import { sendError, sendResponse } from '../../../lib/utils'
import { Poll } from '../types'

export default ({ esClient, config }: { config?: IConfig, esClient?: Elasticsearch, db?: DB }) => {
  const listPollApi = Router()
  const LOGGER_PREFIX = 'POLL LIST: '
  const validationOptions = {
    context: true,
    keyByField: true,
  }

  listPollApi.get('/', auth, validate(listValidation, validationOptions), async (req, res) => {
    const { user }: { user: User } = res.locals
    const { size, sort, subscriptions, searchAfter, isExpired } = req.query
    const subscriptionIds = Number(subscriptions) === 1 ? user.subscriptions : []
    const query = buildQueryForList({
      subscriptions: subscriptionIds,
      searchAfter: searchAfter as string,
      isExpired: isExpired as string,
    })

    try {
      const response = await esClient.search({
        index: config.get('elasticsearch.indices.poll'),
        type: config.get('elasticsearch.types.poll'),
        body: {
          query: query.query,
          sort: getSort(sort as string),
          size: Number(size) > 20 ? 20 : size,
          search_after: query.search_after,
          from: 0,
        },
      })
      response.hits.hits = esClient.extractSourceFromHits(response.hits.hits)
      await assignUserVotes({
        polls: response.hits.hits as Poll[],
        userId: user.id,
        esClient,
        config,
      })
      await assignVotes({
        polls: response.hits.hits as Poll[],
        esClient,
        config,
      })

      return sendResponse({ res, result: [null, response] })
    } catch (error) {
      return sendError({ res, message: error, LOGGER_PREFIX })
    }
  })

  listPollApi.get('/own', auth, validate(ownListValidation, validationOptions), async (req, res) => {
    const { user }: { user: User } = res.locals
    const { searchAfter, sort, size, isExpired } = req.query
    const query = buildQueryForOwnList({
      userId: user.id,
      searchAfter: searchAfter as string,
      isExpired: isExpired as string,
    })

    try {
      const response = await esClient.search({
        index: config.get('elasticsearch.indices.poll'),
        type: config.get('elasticsearch.types.poll'),
        body: query,
        from: 0,
        sort,
        size,
      })
      response.hits.hits = esClient.extractSourceFromHits(response.hits.hits)
      await assignUserVotes({
        polls: response.hits.hits as Poll[],
        userId: user.id,
        esClient,
        config,
      })
      await assignVotes({
        polls: response.hits.hits as Poll[],
        esClient,
        config,
      })

      return sendResponse({ res, result: [null, response] })
    } catch (error) {
      return sendError({ res, message: error, LOGGER_PREFIX })
    }
  })

  listPollApi.get('/own-by-id', auth, validate(ownSingleValidation, validationOptions), async (req, res) => {
    const { user }: { user: User } = res.locals
    const { id } = req.query
    const query = buildQueryForSingle({
      userId: user.id,
      pollId: id as string,
    })

    try {
      const response = await esClient.search({
        index: config.get('elasticsearch.indices.poll'),
        type: config.get('elasticsearch.types.poll'),
        body: query,
        from: 0,
        size: 1,
      })
      response.hits.hits = esClient.extractSourceFromHits(response.hits.hits)
      await assignUserVotes({
        polls: response.hits.hits as Poll[],
        userId: user.id,
        esClient,
        config,
      })
      await assignVotes({
        polls: response.hits.hits as Poll[],
        esClient,
        config,
      })

      return sendResponse({ res, result: [null, response] })
    } catch (error) {
      return sendError({ res, message: error, LOGGER_PREFIX })
    }
  })

  return listPollApi
}
