import { Joi } from 'express-validation'
import { size, requiredString, requiredInteger, interger } from '../../../lib/validation'

const listValidation = {
  query: Joi.object({
    size,
    sort: requiredString,
    subscriptions: requiredInteger,
    searchAfter: requiredInteger,
    isExpired: interger,
  }),
}

const ownListValidation = {
  query: Joi.object({
    size,
    sort: requiredString,
    searchAfter: requiredInteger,
    isExpired: interger,
  }),
}

const ownSingleValidation = {
  query: Joi.object({
    id: requiredInteger,
  }),
}

export {
  listValidation,
  ownListValidation,
  ownSingleValidation,
}
