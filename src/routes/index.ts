import { Router } from 'express'
import userRoutes from './user'
import pollRoutes from './poll'
import complaintRoutes from './complaint'

export default ({ config, esClient, db }) => {
  const routes = Router()

  routes.use('/user', userRoutes({ config, esClient, db }))
  routes.use('/poll', pollRoutes({ config, esClient, db }))
  routes.use('/complaint', complaintRoutes({ config, esClient, db }))

  return routes
}
