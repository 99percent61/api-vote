import chai from 'chai'
import chaiHttp from 'chai-http'
import server from '../../../src'
import User from '../user'
import { deleteUser } from '../../helpers'

const should = chai.should()

chai.use(chaiHttp)

describe('User', () => {
  const user = new User()

  it('Register', function (done) {
    chai.request(server)
      .post('/api/user/registration')
      .send(user.user)
      .end((err, res) => {
        if (err) {
          done(err)
        }
        res.should.have.status(200)
        done()
      })
  })

  it('Create session', function (done) {
    chai.request(server)
      .post('/api/user/session/create')
      .send({ login: user.login, password: user.passwrod })
      .end((err, res) => {
        if (err) {
          done(err)
        }
        user.token = res.get('x-auth-token')
        res.should.have.status(200)
        done()
      })
  })

  it('Create session wrong password', function (done) {
    chai.request(server)
      .post('/api/user/session/create')
      .send({ login: user.login, password: 'Qazw1234' })
      .end((err, res) => {
        if (err) {
          done(err)
        }
        res.should.have.status(403)
        done()
      })
  })

  it('User me', function (done) {
    if (!user.token) {
      this.skip()
    }
    chai.request(server)
      .get('/api/user/me')
      .set('x-access-token', user.token)
      .end((err, res) => {
        if (err) {
          done(err)
        }
        res.should.have.status(200)
        done()
      })
  })

  it('User me invalid token', function (done) {
    if (!user.token) {
      this.skip()
    }
    chai.request(server)
      .get('/api/user/me')
      .set('x-access-token', '1234')
      .end((err, res) => {
        if (err) {
          done(err)
        }
        res.should.have.status(400)
        done()
      })
  })

  after(async () => {
    await deleteUser(user.email)
  })
})
