import chai from 'chai'
import chaiHttp from 'chai-http'
import server from '../../../src'
import User from '../user'
import { createUser, deleteUser, createSession } from '../../helpers'

const should = chai.should()

chai.use(chaiHttp)

describe('Subscribe user', () => {
  const user = new User()
  const user2 = new User()
  user2.login = 'test2'
  user2.email = 'email2@test.ru'

  before(async () => {
    await createUser(user.user)
    user2.userId = await createUser(user2.user)
  })

  it('Subscribe', async function () {
    user.token = await createSession({ login: user.login, password: user.passwrod })
    const res = await chai.request(server)
      .post('/api/user/subscribe')
      .set('x-access-token', user.token)
      .send({ subscribeId: user2.userId })
    
    res.should.have.status(200)
  })

  after(async () => {
    await deleteUser(user.email)
    await deleteUser(user2.email)
  })
})
