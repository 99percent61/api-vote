import chai from 'chai'
import chaiHttp from 'chai-http'
import server from '../../../src'
import User from '../user'
import { createUser, deleteUser, createSession } from '../../helpers'

const should = chai.should()

chai.use(chaiHttp)

describe('Black list', () => {
  const user = new User()

  before(async () => {
    await createUser(user.user)
  })

  it('Add', async function () {
    user.token = await createSession({ login: user.login, password: user.passwrod })
    const res = await chai.request(server)
      .post('/api/user/black-list/add')
      .set('x-access-token', user.token)
      .send({ userId: 1 })
    
    res.should.have.status(200)
  })

  it('Remove', async function () {
    user.token = await createSession({ login: user.login, password: user.passwrod })
    const res = await chai.request(server)
      .post('/api/user/black-list/remove')
      .set('x-access-token', user.token)
      .send({ userId: 1 })
    
    res.should.have.status(200)
  })

  after(async () => {
    await deleteUser(user.email)
  })
})
