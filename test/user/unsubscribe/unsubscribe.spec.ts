import chai from 'chai'
import chaiHttp from 'chai-http'
import server from '../../../src'
import User from '../user'
import { createUser, deleteUser, createSession } from '../../helpers'

const should = chai.should()

chai.use(chaiHttp)

describe('Unsubscribe user', () => {
  const user = new User()
  const user2 = new User()
  user2.login = 'test2'
  user2.email = 'email2@test.ru'

  before(async () => {
    await createUser(user.user)
    user2.userId = await createUser(user2.user)
  })

  it('Unsubscribe', async function () {
    user.token = await createSession({ login: user.login, password: user.passwrod })
    const res = await chai.request(server)
      .post('/api/user/unsubscribe')
      .set('x-access-token', user.token)
      .send({ unsubscribeId: user2.userId })

    res.should.have.status(200)
  })

  after(async () => {
    await deleteUser(user.email)
    await deleteUser(user2.email)
  })
})
