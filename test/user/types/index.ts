export interface User {
  login: string
  email: string
  name: string
  sex: string
  password: string
  repeatPassword?: string
  age: number
}
