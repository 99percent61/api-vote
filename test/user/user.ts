export default class User {
  private localUser = {
    login: 'test',
    email: 'email@test.ru',
    name: 'Test',
    sex: 'male',
    password: 'Qwer1234',
    repeatPassword: 'Qwer1234',
    age: 25,
  }

  private id = null

  private accessToken = ''

  private passwordToken = ''

  public get user () {
    return this.localUser
  }

  public get login () {
    return this.localUser.login
  }

  public set login (value) {
    this.user.login = value
  }

  public get passwrod () {
    return this.localUser.password
  }

  public get token () {
    return this.accessToken
  }

  public set token (token) {
    this.accessToken = token
  }

  public get email () {
    return this.user.email
  }

  public set email (value) {
    this.user.email = value
  }

  public set tokenForPassword (token) {
    this.passwordToken = token
  }

  public get tokenForPassword () {
    return this.passwordToken
  }

  public get newPassword () {
    return 'Qazw1234'
  }

  public get userId () {
    return this.id
  }

  public set userId (value) {
    this.id = value
  }
}
