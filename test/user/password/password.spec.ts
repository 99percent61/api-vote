import chai from 'chai'
import chaiHttp from 'chai-http'
import server from '../../../src'
import User from '../user'
import { createUser, deleteUser, getFromDb, createSession } from '../../helpers'

const should = chai.should()

chai.use(chaiHttp)

describe('User forgot password', () => {
  const user = new User()

  before(async () => {
    await createUser(user.user)
  })

  it('Send code for repair password', function (done) {
    chai.request(server)
      .post('/api/user/password/forgot')
      .send({ email: user.email })
      .end((err, res) => {
        if (err) {
          done(err)
        }
        res.should.have.status(200)
        done()
      })
  })

  it('Confirm code', async function () {
    const record = await getFromDb({ keyForSearch: 'email', value: user.email, table: 'codes' })
    if (!record) {
      throw new Error('Pin did not find')
    }

    try {
      const res = await chai.request(server)
        .post('/api/user/password/confirm-code')
        .send({ email: user.email, code: record.code })

      res.should.have.status(200)
      user.tokenForPassword = res.body[1]
    } catch (error) {
      throw new Error(error)
    }
  })

  it('Create new pass by token', function (done) {
    const token = user.tokenForPassword
    if (!token) {
      done('Token undefined')
    }

    chai.request(server)
      .post('/api/user/password/create-new-pass-by-token')
      .send({ email: user.email, token: token, password: user.newPassword, repeatPassword: user.newPassword })
      .end((err, res) => {
        if (err) {
          done(err)
        }
        res.should.have.status(200)
        done()
      })
  })

  after(async () => {
    await deleteUser(user.email)
  })
})

describe('User change password', () => {
  const user = new User()

  before(async () => {
    await createUser(user.user)
  })

  it('Create new pass by current pass', async function () {
    user.token = await createSession({ login: user.login, password: user.passwrod })
    const res = await chai.request(server)
      .post('/api/user/password/create-new-pass-by-pass')
      .set('x-access-token', user.token)
      .send({ email: user.email, currentPassword: user.passwrod, newPassword: user.newPassword, repeatNewPassword: user.newPassword })

    res.should.have.status(200)
  })

  after(async () => {
    await deleteUser(user.email)
  })
})
