class Poll {
  private localPoll = {
    title: 'Do you like it?',
    time_limit: 24,
    answers: [
      { title: 'Good' },
    ],
  }

  private pollId = null

  public get poll() {
    return this.localPoll
  }

  public get id() {
    return this.pollId
  }

  public set id(id) {
    this.pollId = id
  }
}

export default Poll
