import chai from 'chai'
import chaiHttp from 'chai-http'
import server from '../../src'
import User from '../user/user'
import Poll from './poll'
import { deleteUser, createUser, createSession, deletePoll } from '../helpers'

const should = chai.should()

chai.use(chaiHttp)

describe('Poll', () => {
  const user = new User()
  const poll = new Poll()

  before(async () => {
    await createUser(user.user)
    user.token = await createSession({ login: user.login, password: user.passwrod })
  })

  it('Create', async function () {
    const res = await chai.request(server)
      .put('/api/poll/create')
      .set('x-access-token', user.token)
      .send(poll.poll)
    
    res.should.have.status(200)
    const [error, id] = res.body
    poll.id = id
  })

  it('Update', async function () {
    const res = await chai.request(server)
      .put('/api/poll/update')
      .set('x-access-token', user.token)
      .send({
        id: poll.id,
        title: 'Awsome! What do you think?',
        answers: poll.poll.answers
      })
    
    res.should.have.status(200)
  })

  it('Delete', async function () {
    const res = await chai.request(server)
      .delete(`/api/poll/delete?id=${poll.id}`)
      .set('x-access-token', user.token)
    
    res.should.have.status(200)
  })

  it('Get list', async function () {
    const res = await chai.request(server)
      .get('/api/poll/list?size=20&sort=updated_at&subscriptions=0&searchAfter=0')
      .set('x-access-token', user.token)
    
    res.should.have.status(200)
  })

  it('Get own list', async function () {
    const res = await chai.request(server)
      .get('/api/poll/list/own?size=20&sort=updated_at&searchAfter=0')
      .set('x-access-token', user.token)
    
    res.should.have.status(200)
  })

  after(async () => {
    await deleteUser(user.email)
    await deletePoll(poll.id)
  })
})
