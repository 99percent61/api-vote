import omit from 'lodash/omit'
import request from 'request-promise'
import config from 'config'
import { User } from '../user/types'
import DB from '../../src/lib/db'
import { encryptPassword } from '../../src/lib/auth'

const db = new DB()

async function createUser(user: User): Promise<void> {
  try {
    const record = omit(user, ['repeatPassword'])
    record.password = encryptPassword(record.password)
    const result = await db.query('INSERT into users SET ?', record)
    console.log('User succesfully created')
    return result.insertId
  } catch (error) {
    console.error(error)
    return null
  }
}

async function deleteUser(email: string): Promise<void> {
  try {
    await db.query('DELETE FROM users WHERE email = ?', email)
    console.log('User succesfully deleted')
  } catch (error) {
    console.error(error)
  }
}

async function getFromDb(
  { keyForSearch, value, table }: { keyForSearch: string, value: string, table: string },
): Promise<any> {
  try {
    const result = await db.query(`SELECT * FROM ${table} WHERE ${keyForSearch} = ?`, value)
    if (!result.length) {
      return null
    }
    return result[0]
  } catch (error) {
    console.error(error)
    return null
  }
}

async function createSession({ login, password }: { login: string, password: string }) {
  try {
    const port: string = config.get('server.port')
    const host: string = config.get('server.host')
    const uri = `http://${host}:${port}/api/user/session/create`
    const result = await request({
      uri,
      method: 'POST',
      body: { login, password },
      json: true,
      transform(body, response) {
        const { headers } = response
        return {
          body,
          token: headers['x-auth-token'] || '',
        }
      },
    })
    return result.token
  } catch (error) {
    console.error(error)
    return ''
  }
}

async function deletePoll(id: number) {
  await db.query('DELETE FROM polls WHERE id = ?', id)
}

export {
  createUser,
  deleteUser,
  getFromDb,
  createSession,
  deletePoll,
}
