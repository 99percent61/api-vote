import BaseMigration from './baseMigrate'
import logger from '../../../src/lib/logger'

class VotesMigration extends BaseMigration {
  public async start() {
    const mapping = this.loadMapping()
    await this.createIndex({ name: this.indexName, mapping })
    const existIndex = await this.existIndex(this.indexAlias)
    if (existIndex) {
      this.indices = await this.findAllIndices()
    }
    this.syncVotes()
  }

  public async syncVotes() {
    try {
      const votes = await this.db.query('SELECT * FROM votes')
      logger.info(this.LOGGER_PREFIX, 'Recieved count of votes - ', votes.length)
      await this.createDocuments(votes)
      if (this.indices.length) {
        const indicesForDelete = this.getIndecesForDelete()
        await this.deleteIndices(indicesForDelete)
      }
      await this.putAlias({ alias: this.indexAlias, indexName: this.indexName })
      logger.info(this.LOGGER_PREFIX, 'Sync votes is done')
      this.finish()
    } catch (error) {
      this.finish(error)
    }
  }

  public async startCreateIndexWithMapping() {
    const mapping = this.loadMapping()
    await this.createIndex({ name: this.indexName, mapping })
    await this.putAlias({ alias: this.indexAlias, indexName: this.indexName })
    this.finish()
  }
}

const params = {
  indexAlias: 'votes',
  indexName: `votes_${Date.now()}`,
  entityType: 'vote',
}
const migrate = new VotesMigration(params)
migrate.start()
// migrate.startCreateIndexWithMapping()
