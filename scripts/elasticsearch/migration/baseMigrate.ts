import path from 'path'
import jsonFile from 'jsonfile'
import Elasticsearch from '../../../src/lib/elasticsearch'
import DB from '../../../src/lib/db'
import { MigrationParams } from './types'
import logger from '../../../src/lib/logger'

export default class BaseMigration {
  public esClient = new Elasticsearch()
  public db = new DB()
  public readonly indexAlias: string
  public readonly indexName: string
  public readonly entityType: string
  public indices: string[] = []
  public LOGGER_PREFIX = 'INDEXER: '

  constructor (params: MigrationParams) {
    this.indexAlias = params.indexAlias
    this.indexName = params.indexName
    this.entityType = params.entityType
  }

  public async finish (error?: any): Promise<void> {
    let code = 0
    if (error) {
      logger.error(this.LOGGER_PREFIX, error)
      code = 1
      await this.deleteIndices([this.indexName])
    }
    this.db.close()
    this.esClient.close()
    process.exit(code)
  }

  public async existIndex (name: string): Promise<boolean> {
    try {
      const index = await this.esClient.existIndex(name)
      return index
    } catch (error) {
      this.finish(error)
    }
  }

  public async createIndex ({ name, mapping }: { name: string, mapping?: Record<string, any> }): Promise<void> {
    try {
      await this.esClient.createIndex(name)
      if (mapping) {
        await this.esClient.putMapping({ name, mapping, entityType: this.entityType })
      }
    } catch (error) {
      this.finish(error)
    }
  }

  public async findAllIndices (): Promise<string[]> {
    try {
      const { indices } = await this.esClient.stats()
      return Object.keys(indices)
    } catch (error) {
      this.finish(error)
    }
  }

  public async createDocuments (docs: any[]): Promise<void> {
    if (!docs || !docs.length) {
      return this.finish(new Error('Documents is missing'))
    }

    try {
      for (let doc of docs) {
        await this.esClient.createDocument({ document: doc, indexName: this.indexName, entityType: this.entityType })
        logger.info(this.LOGGER_PREFIX, `Document with id ${doc.id} created in elasticsearch index`)
      }
    } catch (error) {
      this.finish(error)
    }
  }

  public getIndecesForDelete (): string[] {
    return this.indices.filter(name => name !== this.indexName && name.search(this.indexAlias) !== -1)
  }

  public async deleteIndices (indices: string[]): Promise<void> {
    try {
      for (let name of indices) {
        await this.esClient.deleteIndex(name)
      }
      logger.info(this.LOGGER_PREFIX, 'Old indices removed')
    } catch (error) {
      this.finish(error)
    }
  }

  public async putAlias ({ alias, indexName }: { alias: string, indexName: string }): Promise<void> {
    try {
      await this.esClient.putIndexAlias({ alias, indexName })
      logger.info(this.LOGGER_PREFIX, `Alias for index ${indexName} created`)
    } catch (error) {
      this.finish(error)
    }
  }

  public loadMapping (): Record<string, any> {
    const mapping = jsonFile.readFileSync(path.join(__dirname, `../mapping/${this.entityType}.json`))
    return mapping
  }
}
