import BaseMigration from './baseMigrate'
import logger from '../../../src/lib/logger'

class PollsMigration extends BaseMigration {
  public async start() {
    const mapping = this.loadMapping()
    await this.createIndex({ name: this.indexName, mapping })
    const existIndex = await this.existIndex(this.indexAlias)
    if (existIndex) {
      this.indices = await this.findAllIndices()
    }
    this.syncPolls()
  }

  public async syncPolls() {
    try {
      let polls = await this.db.query('SELECT * FROM polls')
      logger.info(this.LOGGER_PREFIX, 'Recieved count of polls - ', polls.length)
      polls = this.preparePolls(polls)
      await this.createDocuments(polls)
      if (this.indices.length) {
        const indicesForDelete = this.getIndecesForDelete()
        await this.deleteIndices(indicesForDelete)
      }
      await this.putAlias({ alias: this.indexAlias, indexName: this.indexName })
      logger.info(this.LOGGER_PREFIX, 'Sync polls is done')
      this.finish()
    } catch (error) {
      this.finish(error)
    }
  }

  private preparePolls(polls: any[]): any[] {
    const preparedPolls = polls.slice()
    preparedPolls.forEach((poll) => {
      try {
        poll.answers = JSON.parse(poll.answers)
        poll.image = JSON.parse(poll.image)
        poll.author = JSON.parse(poll.author)
      } catch (error) {
        logger.error(this.LOGGER_PREFIX, 'Cannot parse field for poll - ', poll, error)
      }
    })
    return preparedPolls
  }

  public async startCreateIndexWithMapping() {
    const mapping = this.loadMapping()
    await this.createIndex({ name: this.indexName, mapping })
    await this.putAlias({ alias: this.indexAlias, indexName: this.indexName })
    this.finish()
  }
}

const params = {
  indexAlias: 'polls',
  indexName: `polls_${Date.now()}`,
  entityType: 'poll',
}
const migrate = new PollsMigration(params)
migrate.start()
// migrate.startCreateIndexWithMapping()
