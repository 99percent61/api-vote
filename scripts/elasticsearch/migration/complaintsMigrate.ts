import BaseMigration from './baseMigrate'
import logger from '../../../src/lib/logger'

class ComplaintsMigration extends BaseMigration {
  public async start() {
    const mapping = this.loadMapping()
    await this.createIndex({ name: this.indexName, mapping })
    const existIndex = await this.existIndex(this.indexAlias)
    if (existIndex) {
      this.indices = await this.findAllIndices()
    }
    this.syncComplaints()
  }

  public async syncComplaints() {
    try {
      const complaints = await this.db.query('SELECT * FROM complaints')
      logger.info(this.LOGGER_PREFIX, `Recieved count of ${this.entityType} - `, complaints.length)
      await this.createDocuments(complaints)
      if (this.indices.length) {
        const indicesForDelete = this.getIndecesForDelete()
        await this.deleteIndices(indicesForDelete)
      }
      await this.putAlias({ alias: this.indexAlias, indexName: this.indexName })
      logger.info(this.LOGGER_PREFIX, 'Sync complaints is done')
      this.finish()
    } catch (error) {
      this.finish(error)
    }
  }

  public async startCreateIndexWithMapping() {
    const mapping = this.loadMapping()
    await this.createIndex({ name: this.indexName, mapping })
    await this.putAlias({ alias: this.indexAlias, indexName: this.indexName })
    this.finish()
  }
}

const params = {
  indexAlias: 'complaints',
  indexName: `complaints_${Date.now()}`,
  entityType: 'complaint',
}
const migrate = new ComplaintsMigration(params)
migrate.start()
// migrate.startCreateIndexWithMapping()
