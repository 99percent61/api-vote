export interface MigrationParams {
  indexAlias: string
  indexName: string
  entityType: string
}
