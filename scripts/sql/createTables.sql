CREATE TABLE users (
  id INT NOT NULL AUTO_INCREMENT,
  login VARCHAR(50) NOT NULL,
  email VARCHAR(100) NOT NULL,
  name VARCHAR(50) NOT NULL,
  password VARCHAR(256) NOT NULL,
  avatar VARCHAR(256),
  black_list JSON,
  settings JSON,
  is_blocked BOOLEAN,
  sex VARCHAR(10),
  age INT NOT NULL,
  subscribers JSON,
  subscriptions JSON,
  terms INT NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE polls (
  id INT NOT NULL AUTO_INCREMENT,
  title VARCHAR(256) NOT NULL,
  author JSON NOT NULL,
  created_at VARCHAR(100),
  updated_at VARCHAR(100),
  expire_at VARCHAR(100),
  is_expired BOOLEAN,
  is_blocked BOOLEAN,
  time_limit INT NOT NULL,
  answers JSON NOT NULL,
  image JSON NOT NULL,
  audio VARCHAR(256),
  video VARCHAR(256),
  PRIMARY KEY (id)
);

CREATE TABLE codes (
  id INT NOT NULL AUTO_INCREMENT,
  email VARCHAR(100) NOT NULL,
  code VARCHAR(10) NOT NULL,
  token VARCHAR(256),
  is_confirmed_code BOOLEAN,
  is_active_token BOOLEAN,
  PRIMARY KEY (id)
);

CREATE TABLE votes (
  id BIGINT NOT NULL AUTO_INCREMENT,
  user_id INT NOT NULL,
  poll_id INT NOT NULL,
  answer_id INT NOT NULL,
  created_at VARCHAR(100),
  PRIMARY KEY (id)
);

CREATE TABLE complaints (
  id BIGINT NOT NULL AUTO_INCREMENT,
  poll_id INT NOT NULL,
  type VARCHAR(100) NOT NULL,
  created_at VARCHAR(100),
  PRIMARY KEY (id)
);
